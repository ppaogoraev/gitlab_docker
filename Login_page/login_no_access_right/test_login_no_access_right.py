import logging
import pytest
import allure
from Login_Page import SearchHelper



@pytest.mark.smoke_test_cv
@allure.feature('Smoke тестирование на Chrome. Авторизация.')
@allure.story('Неуспешная авторизация.Пользователь не имеет прав доступа на сайт.')
@allure.severity('Critical')
def test_login_no_access_right_chrome(test_chrome):
    try:
        logger = logging.getLogger('CV_logger')
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('login_no_access_right.txt')
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(ch)
        logger.addHandler(fh)
        cv_login = SearchHelper(test_chrome)
        cv_login.go_to_login()
        logging.debug('Перешел на страницу логина')
        # Логин и пароль изменять в зависимости от прав данного пользователя на сайте
        cv_login.login_input('ab-no-login')
        logging.debug('Ввел верный логин и пароль пользователя не имеющего прав доступа на сайт')
        cv_login.login_password('951753')
        logging.debug('Ввел верный пароль')
        cv_login.click_login_button()
        logging.debug('Кликнул на кнопку авторизации')
        cv_login.assert_wrong_login()
        logging.debug('Убедился в том, что логин не произошел')
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
        exit(1)
