import logging
import time
import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.common.by import By
from BaseApp import BasePage


class PageLocators:
    # Create locator New system click tale on page

    LOCATOR_CV_CREATE_SYSTEM_BUTTON = (By.XPATH, '//button[contains(text(), "Создать систему")]')
    LOCATOR_CV_INPUT_DATA_NEW_SYSTEM = (By.XPATH, '//input[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_RESOURCE_DATA = (By.XPATH, '//*[contains(text(), "Выберите ресурс")]')
    LOCATOR_CV_CLICK_DROP_RESOURSE = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Питьевая вода")]')
    LOCATOR_CV_TYPE_DATA = (By.XPATH, '//*[contains(text(), "Выберите тип")]')
    LOCATOR_CV_CLICK_DROP_TYPE = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Водопотребление")]')
    LOCATOR_CV_CLICK_CREATE_ZONE = (By.XPATH, '//*[contains(text(), "Создать и добавить зону")]')
    LOCATOR_CV_INPUT_ZONE_NAME = (By.XPATH, '//input[@placeholder="Например, Северный район"]')
    LOCATOR_CV_INPUT_COMMENT_ZONE_NAME = (By.XPATH, '//div[2]/div/textarea[@placeholder="Краткое описание"]')
    LOCATOR_CV_CLICK_SAVE_ZONE_DATA = (By.XPATH, '//*[contains(text(), "Сохранить зону")]')
    LOCATOR_CV_CLICK_SAVE_SYSTEM_BUTTON = (By.XPATH, '//*[contains(text(), "Сохранить")]')
    LOCATOR_CV_ASSERT_CREATE_TILE_SYSTEM = (By.XPATH, '//*[contains(text(), "test2_Проверка создания новой системы")]')
    LOCATOR_CV_CLICK_TALE_SYSTEM = (By.XPATH, '//*[contains(text(), "test2_Проверка создания новой системы")]')
    LOCATOR_CV_CLICK_DELETE_SYSTEM_BUTTON = (By.XPATH, '//*[@class="btn btn-fargo" and contains(text(), "Удалить")]')
    LOCATOR_CV_CLICK_CONFIRM_DELETE_SYSTEM = (By.XPATH, '//button[contains(text(), "Да, удалить")]')
    LOCATOR_CV_ASSERT_DELETE_TILE_SYSTEM = (By.XPATH, '//*[contains(text(), "test2_Проверка создания новой системы")]')

    # Create locator New system click tale on page
    LOCATOR_CV_CREATE_SYSTEM_TILE = (By.XPATH, '//*[@class="system-panel" and contains(text(), "Создать систему")]')
    LOCATOR_CV_INPUT_DATA_NEW_SYSTEM_TALE = (By.XPATH, '//input[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_RESOURCE_DATA_TILE = (By.XPATH, '//*[contains(text(), "Выберите ресурс")]')
    LOCATOR_CV_CLICK_DROP_RESOURSE_TILE = (
        By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Питьевая вода")]')
    LOCATOR_CV_TYPE_DATA_TILE = (By.XPATH, '//*[contains(text(), "Выберите тип")]')
    LOCATOR_CV_CLICK_DROP_TYPE_TILE = (
        By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Водопотребление")]')
    LOCATOR_CV_CLICK_CREATE_ZONE_TILE = (By.XPATH, '//*[contains(text(), "Создать и добавить зону")]')
    LOCATOR_CV_INPUT_ZONE_NAME_TILE = (By.XPATH, '//input[@placeholder="Например, Северный район"]')
    LOCATOR_CV_INPUT_COMMENT_ZONE_NAME_TILE = (By.XPATH, '//div[2]/div/textarea[@placeholder="Краткое описание"]')
    LOCATOR_CV_CLICK_SAVE_ZONE_DATA_TILE = (By.XPATH, '//*[contains(text(), "Сохранить зону")]')
    LOCATOR_CV_CLICK_SAVE_SYSTEM_BUTTON_TILE = (By.XPATH, '//*[contains(text(), "Сохранить")]')
    LOCATOR_CV_ASSERT_CREATE_TILE_SYSTEM_TILE = (
        By.XPATH, '//*[contains(text(), "test2_Проверка создания новой системы")]')
    LOCATOR_CV_CLICK_TALE_SYSTEM_TILE = (By.XPATH, '//*[contains(text(), "test2_Проверка создания новой системы")]')
    LOCATOR_CV_CLICK_DELETE_SYSTEM_BUTTON_TILE = (
        By.XPATH, '//*[@class="btn btn-fargo" and contains(text(), "Удалить")]')
    LOCATOR_CV_CLICK_CONFIRM_DELETE_SYSTEM_TILE = (By.XPATH, '//button[contains(text(), "Да, удалить")]')
    LOCATOR_CV_GO_MAIN_PAGE = 'https://ui-demo.water.rusatom.dev/structure/systems'
    LOCATOR_INPUT_NAME_SYSTEM_SEARCH_BAR = (By.XPATH,
                                            '//*[@id="structure-ui-app"]/div[1]/div/div[2]/div[1]/div/div[2]/input')
    LOCATOR_CV_ASSERT_DELETE_TILE_SYSTEM_TILE = (
        By.XPATH, '//*[contains(text(), "test2_Проверка создания новой системы")]')
    # Create locator create System Source
    LOCATOR_CV_CLICK_LINK_SOURCE = (By.XPATH, '//*[contains(text(), "Источники")]')
    LOCATOR_CV_CLICK_ADD_SOURCE_BUTTON = (By.XPATH, '//*[contains(text(), "Добавить источник")]')
    LOCATOR_CV_ADD_DATA_SOURCE_NAME = (By.XPATH, '//*[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_ADD_DATA_SOURCE_TYPE = (By.XPATH, '//*[@class="radio-button-text" and contains(text(),"Подземный")]')
    LOCATOR_CV_ADD_DATA_DROP_ZONE = (By.XPATH, '//div[4]/div/div/div[2]/div/i[@class="wi-arrow-down_16"]')
    LOCATOR_CV_ADD_DATA_NAME_ZONE = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Ленинский район")]')
    LOCATOR_CV_ADD_DATA_DROP_STAGE = (By.XPATH, '//*[contains(text(), "Выберите этап")]')
    LOCATOR_CV_ADD_DATA_NAME_STAGE = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Водоподготовка")]')
    LOCATOR_CV_ADD_DATA_LOCATION = (By.XPATH, '//div[6]/div/div/div[1]/input[@class="ws-input"]')
    LOCATOR_CV_CLICK_DROP_LIST_LOCATION = (By.XPATH, '//div[6]/div/div/div[3]/div/div[1]/span')
    LOCATOR_CV_ADD_DATA_COMMENTS_SOURCE = (By.XPATH, '//textarea[@placeholder="Краткое описание"]')
    LOCATOR_CV_SAVE_DATA_SOURCE = (By.XPATH, '//button[contains(text(), "Сохранить")]')
    # Create locator Water network object
    LOCATOR_CV_CLICK_LINK_WATER_NETWORK_OBJECT = (By.XPATH, '//*[contains(text(), "Объекты сети")]')
    LOCATOR_CV_CLICK_CREATE_NETWORK_OBJECT = (By.XPATH, '//*[contains(text(), "Добавить объект сети")]')
    LOCATOR_CV_ADD_NETWORK_OBJECT_NAME = (By.XPATH, '//*[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_CLICK_DROP_TYPE_OBJECT = (By.XPATH, '//*[contains(text(), "Объект сети (прочий)")]')
    LOCATOR_CV_CLICK_NAME_DROP_TYPE_OBJECT = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), " Насосная '
                                                        'станция ")]')
    LOCATOR_CV_CLICK_DROP_ZONE_OBJECT = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_INPUT_ZONE_OBJECT_NAME = (By.XPATH, '//div[1]/div/div/div/div/div[1]/div[4]/div/div/div[2]/div')

    LOCATOR_CV_CLICK_NAME_ZONE_OBJECT = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Ленинский '
                                                   'район")]')
    LOCATOR_CV_ClICK_DROP_STAGE_OBJECT = (By.XPATH, '//*[contains(text(), "Выберите этап")]')
    LOCATOR_CV_ADD_DATA_NAME_STAGE_OBJECT = (By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), '
                                                       '"Транспортировка")]')
    LOCATOR_CV_CLICK_DROP_LOCATION_OBJECT = (By.XPATH, '//div[6]/div/div/div[1]/input[@class="ws-input"]')
    LOCATOR_CV_ADD_DATA_LOCATION_OBJECT = (By.XPATH, '//div[6]/div/div/div[1]/input')
    LOCATOR_CV_CLICK_DROP_LIST_LOCATION_OBJECT = (By.XPATH, '//div[6]/div/div/div[3]/div/div[1]/span')
    LOCATOR_CV_SAVE_DATA_OBJECT = (By.XPATH, '//button[contains(text(), "Сохранить")]')
    LOCATOR_CV_ASSERT_CREATE_NETWORK_OBJECT = (By.XPATH, '//*[contains(text(), "НС-100")]')
    # Create locator Consumer

    LOCATOR_CV_CLICK_LINK_CONSUMER = (By.XPATH, '//*[contains(text(), "Потребители")]')
    LOCATOR_CV_CLICK_CREATE_CONSUMER = (By.XPATH, '//*[contains(text(), "Добавить потребителя")]')
    LOCATOR_CV_INPUT_CONSUMER_NAME = (By.XPATH, '//*[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_CLICK_DROP_ZONE_CONSUMER = (By.XPATH, '//div[3]/div/div/div[2]/div/i')
    LOCATOR_CV_CLICK_NAME_DROP_ZONE_CONSUMER = (
        By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Ленинский район")]')
    LOCATOR_CV_CLICK_DROP_LOCATION_CONSUMER = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_ADD_DATA_LOCATION_CONSUMER = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_CLICK_DROP_LIST_LOCATION_CONSUMER = (By.XPATH, '//div[4]/div/div/div[3]/div')
    LOCATOR_CV_SAVE_DATA_CONSUMER = (By.XPATH, '//button[contains(text(), "Сохранить")]')
    LOCATOR_CV_ASSERT_CREATE_CONSUMER = (By.XPATH, '//*[contains(text(), "Химчистка Снежинка")]')
    # Create locator Pipeline section

    LOCATOR_CV_CLICK_LINK_PIPELINE_SECTION = (By.XPATH, '//*[contains(text(), "Участки сети")]')
    LOCATOR_CV_CLICK_CREATE_PIPELINE_SECTION = (By.XPATH, '//*[contains(text(), "Добавить участок сети")]')
    LOCATOR_CV_INPUT_PIPELINE_SECTION_NAME = (By.XPATH, '//*[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_CLICK_PIPELINE_SECTION_TYPE = (By.XPATH, '//*[contains(text(), "Активный")]')
    LOCATOR_CV_CLICK_DROP_START_SECTION = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_ADD_DATA_DROP_START_SECTION = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_CLICK_DROP_LIST_START_SECTION = (By.XPATH, '//div[4]/div/div/div[3]/div')
    LOCATOR_CV_CLICK_DROP_END_SECTION = (By.XPATH, '//div[5]/div/div/div[1]/input')
    LOCATOR_CV_ADD_DATA_DROP_END_SECTION = (By.XPATH, '//div[5]/div/div/div[1]/input')
    LOCATOR_CV_CLICK_DROP_LIST_END_SECTION = (By.XPATH, '//div[5]/div/div/div[3]/div')
    LOCATOR_CV_SAVE_DATA_SECTION = (By.XPATH, '//button[contains(text(), "Сохранить")]')
    LOCATOR_CV_ASSERT_CREATE_SECTION = (By.XPATH, '//*[contains(text(), "Трубопровод 150")]')
    # Create locator Buildings

    LOCATOR_CV_CLICK_LINK_BUILDINGS = (By.XPATH, '//*[contains(text(), "Здания")]')
    LOCATOR_CV_CLICK_ADD_BUILDINGS = (By.XPATH, '//*[contains(text(), "Добавить здание")]')
    LOCATOR_CV_INPUT_BUILDINGS_NAME = (By.XPATH, '//*[@placeholder="Например, Система водоснабжения"]')
    LOCATOR_CV_CLICK_DROP_ZONE_BUILDINGS = (By.XPATH, '//div[3]/div/div/div[2]/div/i')
    LOCATOR_CV_CLICK_NAME_DROP_ZONE_BUILDINGS = (
        By.XPATH, '//*[@class="ws-dropdown-item" and contains(text(), "Ленинский район")]')
    LOCATOR_CV_CLICK_DROP_LOCATION_BUILDINGS = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_ADD_DATA_LOCATION_BUILDINGS = (By.XPATH, '//div[4]/div/div/div[1]/input')
    LOCATOR_CV_CLICK_DROP_LIST_LOCATION_BUILDINGS = (By.XPATH, '//div[4]/div/div/div[3]/div')
    LOCATOR_CV_SAVE_DATA_BUILDINGS = (By.XPATH, '//button[contains(text(), "Сохранить")]')
    LOCATOR_CV_ASSERT_CREATE_BUILDINGS = (By.XPATH, '//*[contains(text(), "Хозяйственный корпус 2А")]')


    # Create locator Params and data

    LOCATOR_CV_CLICK_LINK_PARAMS_DATA = (By.XPATH, '//*[contains(text(), "Параметры и данные")]')
    LOCATOR_CV_CLICK_INITIALIZE = (By.XPATH, '//button[contains(text(), "Инициализировать")]')
    LOCATOR_CV_ASSERT_CREATE_PARAMS_DATA = (By.XPATH, '//button[contains(text(), "Новый параметр")]')



class SearchHelperCNS(BasePage):
    # Create New system click button on page

    def click_create_system_button(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CREATE_SYSTEM_BUTTON):
                with allure.step('Клик на кнопку Создать систему. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку Создать систему. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку Создать систему. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку Создать систему. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_name_new_system(self):
        try:
            global name_system
            name_system = self.find_element(PageLocators.LOCATOR_CV_INPUT_DATA_NEW_SYSTEM).send_keys(
                "test2_Проверка создания "
                "новой системы")
            with allure.step('Заполнение данных "Наименование". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_resource_new_system(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_RESOURCE_DATA)
            with allure.step('Нажимется выпадающее меню "Ресурс". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Ресурс". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется выпадающее меню "Ресурс". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Ресурс". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_resource(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_RESOURSE):
                with allure.step('Клик на пункт "Питьевая вода". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Питьевая вода" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Питьевая вода" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Питьевая вода". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_type_data(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_TYPE_DATA)
            with allure.step('Нажимется выпадающее меню "Тип". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Тип". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется выпадающее меню "Тип". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Тип". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_type(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_TYPE):
                with allure.step('Клик на выпадающий список Ресурс. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на на выпадающий список Ресурс. '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на выпадающий список Ресурс. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на выпадающий список Ресурс. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_zone(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CREATE_ZONE)
            with allure.step('Нажимется выпадающее меню "Создать и добавиь зону". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Создать и добавиь '
                                                                        'зону". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется выпадающее меню "Создать и добавиь зону". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Создать и добавиь '
                                                                        'зону". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_zone_name(self):
        try:

            if self.find_element(PageLocators.LOCATOR_CV_INPUT_ZONE_NAME).send_keys("Ленинский район"):
                with allure.step('Заполнение данных "Наименование зоны водоснабжения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование зоны '
                                                                            'водоснабжения".'' Успешный.',
                                  attachment_type=AttachmentType.PNG)

        except:
            with allure.step('Заполнение данных "Наименование зоны водоснабжения". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Наименование зоны водоснабжения". ''Провален.',
                              attachment_type=AttachmentType.PNG)

            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_comment_zone_name(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_INPUT_COMMENT_ZONE_NAME).send_keys("ТЭЦ-5"):
                with allure.step('Заполнение данных "Комментарий". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение данных "Комментарий".''Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Комментарий". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Комментарий". ''Провален.',
                              attachment_type=AttachmentType.PNG)

            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_zone_data(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_SAVE_ZONE_DATA):
                with allure.step('Клик на кнопку "Сохранить зону". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на кнопку "Сохранить зону". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Сохранить зону". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Сохранить зону". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_system_button(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_SAVE_SYSTEM_BUTTON):
                with allure.step('Клик на кнопку "Сохранить". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Сохранить". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_tile_system(self):
        try:
            time.sleep(1.0)
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_TILE_SYSTEM):
                with allure.step(
                        'Убедился в создании системы, найдя плитку с её названием на главной странице. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании системы, '
                                                                            'найдя плитку с её названием'
                                                                            ' на главной странице. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в создании системы, найдя плитку с её названием на главной странице. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании системы, '
                                                                        'найдя плитку с её названием'
                                                                        ' на главной странице. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_tiles_system(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_TALE_SYSTEM):
                with allure.step('Клик на плитку новой системы. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик наплитку новой системы. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на плитку новой системы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на плитку новой системы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_delete_system_button(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DELETE_SYSTEM_BUTTON):
                with allure.step('Клик на кнопку "Удалить". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Удалить". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Удалить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Удалить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_confirm_delete_system(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CONFIRM_DELETE_SYSTEM):
                with allure.step('Клик на кнопку "Да, удалить". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Да, удалить". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Да, удалить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Да, удалить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_delete_tile_system(self):
        try:
            if self.invisible_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_TILE_SYSTEM):
                with allure.step(
                        'Убедился в отсутсвии системы, найдя плитку с её названием на главной странице. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в отсутсвии системы, '
                                                                            'найдя плитку с её названием'
                                                                            ' на главной странице. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Убедился в отсутсвии системы, найдя плитку с её названием на главной странице. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в отсутсвии системы, '
                                                                        'найдя плитку с её названием'
                                                                        ' на главной странице. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create New system click tale on page

    def click_create_system_tale(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CREATE_SYSTEM_TILE):
                with allure.step('Клик на плитку Создать систему. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на плитку Создать систему. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на плитку Создать систему. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на плитку Создать систему. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_name_new_system_tale(self):
        try:
            global name_system
            name_system = self.find_element(PageLocators.LOCATOR_CV_INPUT_DATA_NEW_SYSTEM).send_keys(
                "test2_Проверка создания "
                "новой системы")
            with allure.step('Заполнение данных "Наименование". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_resource_new_system_tile(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_RESOURCE_DATA_TILE)
            with allure.step('Нажимется выпадающее меню "Ресурс". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Ресурс". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется выпадающее меню "Ресурс". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Ресурс". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_resource_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_RESOURSE_TILE):
                with allure.step('Клик на пункт "Питьевая вода". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Питьевая вода" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Питьевая вода" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Питьевая вода". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_type_data_tile(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_TYPE_DATA_TILE)
            with allure.step('Нажимется выпадающее меню "Тип". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Тип". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется выпадающее меню "Тип". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Тип". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_type_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_TYPE_TILE):
                with allure.step('Клик на выпадающий список Ресурс. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на на выпадающий список Ресурс. '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на выпадающий список Ресурс. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на выпадающий список Ресурс. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_zone_tile(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CREATE_ZONE_TILE)
            with allure.step('Нажимется выпадающее меню "Создать и добавиь зону". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Создать и добавиь '
                                                                        'зону". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется выпадающее меню "Создать и добавиь зону". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется выпадающее меню "Создать и добавиь '
                                                                        'зону". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_zone_name_tile(self):
        try:

            if self.find_element(PageLocators.LOCATOR_CV_INPUT_ZONE_NAME_TILE).send_keys("Ленинский район"):
                with allure.step('Заполнение данных "Наименование зоны водоснабжения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование зоны '
                                                                            'водоснабжения".'' Успешный.',
                                  attachment_type=AttachmentType.PNG)

        except:
            with allure.step('Заполнение данных "Наименование зоны водоснабжения". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Наименование зоны водоснабжения". ''Провален.',
                              attachment_type=AttachmentType.PNG)

            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_comment_zone_name_tile(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_INPUT_COMMENT_ZONE_NAME_TILE).send_keys("ТЭЦ-5"):
                with allure.step('Заполнение данных "Комментарий". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение данных "Комментарий".''Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Комментарий". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Комментарий". ''Провален.',
                              attachment_type=AttachmentType.PNG)

            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_zone_data_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_SAVE_ZONE_DATA_TILE):
                with allure.step('Клик на кнопку "Сохранить зону". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на кнопку "Сохранить зону". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Сохранить зону". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Сохранить зону". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_system_button_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_SAVE_SYSTEM_BUTTON_TILE):
                with allure.step('Клик на кнопку "Сохранить". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Сохранить". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_tile_system_tile(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_TILE_SYSTEM_TILE):
                with allure.step(
                        'Убедился в создании системы, найдя плитку с её названием на главной странице. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании системы, '
                                                                            'найдя плитку с её названием'
                                                                            ' на главной странице. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в создании системы, найдя плитку с её названием на главной странице. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании системы, '
                                                                        'найдя плитку с её названием'
                                                                        ' на главной странице. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_tiles_system_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_TALE_SYSTEM_TILE):
                with allure.step('Клик на плитку новой системы. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик наплитку новой системы. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на плитку новой системы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на плитку новой системы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_delete_system_button_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DELETE_SYSTEM_BUTTON_TILE):
                with allure.step('Клик на кнопку "Удалить". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Удалить". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Удалить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Удалить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_confirm_delete_system_tile(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CONFIRM_DELETE_SYSTEM_TILE):
                with allure.step('Клик на кнопку "Да, удалить". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Да, удалить". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Да, удалить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Да, удалить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def go_to_main_page(self):
        try:
            if self.driver.get(PageLocators.LOCATOR_CV_GO_MAIN_PAGE):
                with allure.step('Переход на страницу "Инженерные сооружения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Переход на страницу "Инженерные сооружения". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Переход на страницу "Инженерные сооружения". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу "Инженерные сооружения". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_name_system_search_bar(self):
        try:
            if self.find_element(PageLocators.LOCATOR_INPUT_NAME_SYSTEM_SEARCH_BAR).send_keys(
                    "test2_Проверка создания новой системы"):
                with allure.step('Заполнение строки поиска системы. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение строки поиска системы. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение строки поиска системы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных строки поиска системы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_delete_tile_system_tile(self):
        try:
            if self.invisible_element(PageLocators.LOCATOR_CV_ASSERT_DELETE_TILE_SYSTEM_TILE):
                with allure.step(
                        'Убедился в отсутсвии системы, не найдя плитку с её названием на главной странице. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в отсутсвии системы, '
                                                                            'не найдя плитку с её названием'
                                                                            ' на главной странице. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Убедился в отсутсвии системы, не найдя плитку с её названием на главной странице. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в отсутсвии системы, '
                                                                        'не найдя плитку с её названием'
                                                                        ' на главной странице. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create System source

    def click_link_source(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_LINK_SOURCE):
                with allure.step('Клик на строку "Источкики". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Источкики". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Источкики". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Источкики". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_system_source(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_ADD_SOURCE_BUTTON):
                with allure.step('Клик на кнопку Добавить источник. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку Добавить источник. '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку Добавить источник. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку Добавить источник. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_source_name(self):
        try:
            time.sleep(1.0)
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_SOURCE_NAME).send_keys(
                    "Водоподъём №12"):
                with allure.step('Заполнение данных "Наименование". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Наименование". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_source_type(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_SOURCE_TYPE)
            with allure.step('Нажимется чек-бокс "Подземный". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется чек-бокс "Подземный". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется чек-бокс "Подземный". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется чек-бокс "Подземный". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_zone(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_DROP_ZONE):
                with allure.step('Клик на пункт "Выберите зону водоснабжения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Выберите зону '
                                                                            'водоснабжения" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Выберите зону водоснабжения". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Выберите зону водоснабжения". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_name_zone(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_NAME_ZONE):
                with allure.step('Клик на пункт "Ленинский район". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Ленинский район" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_stage(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_DROP_STAGE):
                with allure.step('Клик на пункт "Этап". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Этап" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Этап". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Этап". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_name_stage(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_NAME_STAGE):
                with allure.step('Клик на пункт "Водоподготовка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Водоподготовка" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Водоподготовка" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Водоподготовка". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_location(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_LOCATION):
                with allure.step('Клик на пункт "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адрес" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адреся". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_location(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_LOCATION).send_keys(
                    "Удмуртская респ., р-н Кезский, д Удмурт-Зязьгор"):
                with allure.step('Заполнение данных "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_list_location(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LIST_LOCATION):
                with allure.step(
                        'Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., р-н Кезский, '
                        'д. Удмурт-Зязьгор". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                       'р-н Кезский, д. Удмурт-Зязьгор" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., р-н Кезский, '
                    'д. Удмурт-Зязьгор". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                   'р-н Кезский, д. Удмурт-Зязьгор". '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_comments_source(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_COMMENTS_SOURCE).send_keys(
                    "Кама-3"):
                with allure.step('Заполнение данных "Комментарий". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Комментарий". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Комментарий". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Комментарий". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_data_source(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_SAVE_DATA_SOURCE)
            with allure.step('Нажимется кнопка "Сохранить". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется кнопка "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create Water network object

    def click_link_water_network_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_LINK_WATER_NETWORK_OBJECT):
                with allure.step('Клик на строку "Объекты сети". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Объекты сети". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Объекты сети". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Объекты сети". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_water_network_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CREATE_NETWORK_OBJECT):
                with allure.step('Клик на кнопку "Добавить объект сети". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Добавить объект сети". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Добавить объект сети". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Добавить объект сети". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_water_network_object_name(self):
        try:
            time.sleep(1.0)
            if self.find_element(PageLocators.LOCATOR_CV_ADD_NETWORK_OBJECT_NAME).send_keys(
                    "НС-100"):
                with allure.step('Заполнение данных "Наименование" "НС-100". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование" '
                                                                            '"НС-100". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Наименование" "НС-100". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование" "НС-100". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_type_object(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_TYPE_OBJECT):
                with allure.step('Клик на пункт "Тип объекта". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Тип объекта" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Тип объекта". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Тип объекта". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_name_drop_type_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_NAME_DROP_TYPE_OBJECT):
                with allure.step('Клик на пункт "Насосная станция". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Насосная станция" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Насосная станция" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Насосная станция". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_zone_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_ZONE_OBJECT):
                with allure.step('Клик на пункт "Зона водоснабжения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Зона водоснабжения" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Зона водоснабжения" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Зона водоснабжения". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_name_zone_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_NAME_ZONE_OBJECT):
                with allure.step('Клик на пункт "Ленинский район". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Ленинский район" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_stage_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ClICK_DROP_STAGE_OBJECT):
                with allure.step('Клик на пункт "Этап". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Этап" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Этап". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Этап". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_name_stage_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_ADD_DATA_NAME_STAGE_OBJECT):
                with allure.step('Клик на пункт "Транспортировка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Транспортировка" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Транспортировка" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Транспортировка". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_location_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LOCATION_OBJECT):
                with allure.step('Клик на пункт "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адрес" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адреся". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_location_object(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_LOCATION_OBJECT).send_keys(
                    "Удмуртская респ., р-н Кезский, тер. СПК Свобода Удмуртский Зязьгор"):
                with allure.step('Заполнение данных "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_list_location_object(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LIST_LOCATION_OBJECT):
                with allure.step(
                        'Клик на пункт в списке "Адрес", должна отобразиться информация: "Удмуртская респ., '
                        'р-н Кезский, '
                        'д. Удмурт-Зязьгор". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                       'р-н Кезский, д. Удмурт-Зязьгор" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., р-н Кезский, '
                    'д. Удмурт-Зязьгор". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                   'р-н Кезский, д. Удмурт-Зязьгор". '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_data_object(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_SAVE_DATA_OBJECT)
            with allure.step('Нажимется кнопка "Сохранить". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется кнопка "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_network_object(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_NETWORK_OBJECT):
                with allure.step(
                        'Убедился в создании Объекта сети, найдя строку с её названием. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Объекта сети, '
                                                                            'найдя строку с её названием'
                                                                            '. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в создании Объекта сети, найдя пстроку с её названием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Объекта сети, '
                                                                        'найдя строку с её названием'
                                                                        '. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create Consumer

    def click_link_consumer(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_LINK_CONSUMER):
                with allure.step('Клик на строку "Потребители". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Потребители". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Потребители". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Потребители". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_consumer(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CREATE_CONSUMER):
                with allure.step('Клик на строку "Добавить потребителя". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Добавить потребителя". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Добавить потребителя". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Добавить потребителя". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_consumer_name(self):
        try:
            time.sleep(1.0)
            if self.find_element(PageLocators.LOCATOR_CV_INPUT_CONSUMER_NAME).send_keys(
                    "Химчистка Снежинка"):
                with allure.step('Заполнение данных "Наименование" "Химчистка Снежинка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование" '
                                                                            '"Химчистка Снежинка". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Наименование" "Химчистка Снежинка". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Наименование" "Химчистка '
                                                                        'Снежинка". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_zone_consumer(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_ZONE_CONSUMER):
                with allure.step('Клик на пункт "Зона водоснабжения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Зона водоснабжения" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Зона водоснабжения". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Зона водоснабжения". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_name_drop_zone_consumer(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_NAME_DROP_ZONE_CONSUMER):
                with allure.step('Клик на пункт "Ленинский район". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Ленинский район" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_location_consumer(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LOCATION_CONSUMER):
                with allure.step('Клик на пункт "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адрес" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адреся". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_location_consumer(self):
        try:
            time.sleep(2.0)
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_LOCATION_CONSUMER).send_keys(
                    "Удмуртская респ., р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная"):
                with allure.step('Заполнение данных "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_list_location_consumer(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LIST_LOCATION_CONSUMER):
                with allure.step(
                        'Клик на пункт в списке "Адрес", должна отобразиться информация: "Удмуртская респ., '
                        'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                       'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., р-н Кезский, '
                    'д. Удмурт-Зязьгор, ул. Юбилейная". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                   'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная". '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_data_consumer(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_SAVE_DATA_CONSUMER)
            with allure.step('Нажимется кнопка "Сохранить". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется кнопка "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_consumer(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_CONSUMER):
                with allure.step(
                        'Убедился в создании Потребителя, найдя строку с названием. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Потребителя, '
                                                                            'найдя строку с названием'
                                                                            '. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в создании Потребителя, найдя строку с названием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Потребителя, '
                                                                        'найдя строку с названием'
                                                                        '. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create Pipeline section

    def click_link_pipeline_section(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_LINK_PIPELINE_SECTION):
                with allure.step('Клик на строку "Участки сети". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Участки сети". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Участки сети". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Участки сети". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_pipeline_section(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_CREATE_PIPELINE_SECTION):
                with allure.step('Клик на активную строку "Добавить участок сети". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на активную строку "Добавить участок сети". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Добавить участок сети". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на активную строку "Добавить участок сети". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_pipeline_section_name(self):
        try:
            time.sleep(1.0)
            if self.find_element(PageLocators.LOCATOR_CV_INPUT_PIPELINE_SECTION_NAME).send_keys(
                    "Трубопровод 150"):
                with allure.step('Заполнение данных "Наименование" "Трубопровод 150". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение данных "Наименование" "Трубопровод 150". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Наименование" "Трубопровод 150". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Наименование" "Трубопровод 150". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_pipeline_section_type(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_PIPELINE_SECTION_TYPE)
            with allure.step('Нажимется чек-бокс "Активный". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется чек-бокс "Активный". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется чек-бокс "Активный". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется чек-бокс "Активный". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_start_section(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_START_SECTION):
                with allure.step('Клик на поле "Начало участка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на поле "Начало участка" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на поле "Начало участка". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на поле "Начало участка". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_drop_start_section(self):
        try:
            time.sleep(2.0)
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_LOCATION_CONSUMER).send_keys(
                    "НС-100"):
                with allure.step('Заполнение данных "Начало участка" "НС-100". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение данных "Начало участка" "НС-100". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Начало участка" "НС-100". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Начало участка" "НС-100". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_list_start_section(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LIST_START_SECTION):
                with allure.step(
                        'Клик на пункт в списке "НС-100". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "НС-100". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Клик на пункт "НС-100". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на пункт  "НС-100". '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_end_section(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_END_SECTION):
                with allure.step('Клик на поле "Окончание участка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на поле "Окончание участка" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на поле "Окончание участка". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на поле "Окончание участка". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_drop_end_section(self):
        try:
            time.sleep(2.0)
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_DROP_END_SECTION).send_keys(
                    "Химчистка Снежинка"):
                with allure.step('Заполнение данных "Окончание участка" "Химчистка Снежинка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение данных "Окончание участка" "Химчистка Снежинка". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Окончание участка" "Химчистка Снежинка". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Окончание участка" "Химчистка Снежинка". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_list_end_section(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LIST_END_SECTION):
                with allure.step(
                        'Клик на пункт в списке "Окончание участка", должна отобразиться информация: "Химчистка '
                        'Снежинка". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Окончание участка", должна отобразиться информация: "Химчистка '
                                       'Снежинка". Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Клик на пункт "Окончание участка", должна отобразиться информация: "Химчистка Снежинка"". '
                    'Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на пункт "Окончание участка", должна отобразиться информация: "Химчистка '
                                   'Снежинка". '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_data_section(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_SAVE_DATA_SECTION)
            with allure.step('Нажимется кнопка "Сохранить". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется кнопка "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_section(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_SECTION):
                with allure.step(
                        'Убедился в создании участка сети "Трубопровод 150", найдя строку с названием. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Пучастка сети '
                                                                            '"Трубопровод 150", '
                                                                            'найдя строку с названием'
                                                                            '. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в создании участка сети "Трубопровод 150", найдя строку с названием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании участка сети '
                                                                        '"Трубопровод 150", '
                                                                        'найдя строку с названием'
                                                                        '. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create buildings

    def click_link_buildings(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_LINK_BUILDINGS):
                with allure.step('Клик на строку "Здания". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Здания". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Здания". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Здания". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_create_buildings(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_ADD_BUILDINGS):
                with allure.step('Клик на активную строку "Добавить здание". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на активную строку "Добавить здание". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку Добавить здание. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на активную строку "Добавить здание". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def input_buildings_name(self):
        try:
            time.sleep(1.0)
            if self.find_element(PageLocators.LOCATOR_CV_INPUT_BUILDINGS_NAME).send_keys(
                    "Хозяйственный корпус 2А"):
                with allure.step('Заполнение данных "Наименование" "Хозяйственный корпус 2А"". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Заполнение данных "Наименование" "Хозяйственный корпус 2А"". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Наименование" "Хозяйственный корпус 2А"". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение данных "Наименование" "Хозяйственный корпус 2А"". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_zone_buildings(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_ZONE_BUILDINGS):
                with allure.step('Клик на пункт "Зона водоснабжения". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Зона водоснабжения" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Зона водоснабжения". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Зона водоснабжения". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_name_drop_zone_buildings(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_NAME_DROP_ZONE_BUILDINGS):
                with allure.step('Клик на пункт "Ленинский район". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район" '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Ленинский район" Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Ленинский район". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_location_buildings(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LOCATION_BUILDINGS):
                with allure.step('Клик на пункт "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адрес" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на пункт "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на пункт "Адреся". '
                                                                        'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def add_data_location_buildings(self):
        try:
            time.sleep(2.0)
            if self.find_element(PageLocators.LOCATOR_CV_ADD_DATA_LOCATION_BUILDINGS).send_keys(
                    "Удмуртская респ., р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная"):
                with allure.step('Заполнение данных "Адрес". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение данных "Адрес". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Заполнение данных "Адрес". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_drop_list_location_buildings(self):
        try:
            time.sleep(2.0)
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_DROP_LIST_LOCATION_BUILDINGS):
                with allure.step(
                        'Клик на пункт в списке "Адрес", должна отобразиться информация: "Удмуртская респ., '
                        'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская '
                                       'респ., '
                                       'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная" Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step(
                    'Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., р-н Кезский, '
                    'д. Удмурт-Зязьгор, ул. Юбилейная". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на пункт "Адрес", должна отобразиться информация: "Удмуртская респ., '
                                   'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная". '
                                   'Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_save_data_buildings(self):
        try:
            self.find_clickable_element(PageLocators.LOCATOR_CV_SAVE_DATA_BUILDINGS)
            with allure.step('Нажимется кнопка "Сохранить". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Нажимется кнопка "Сохранить". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Нажимется кнопка "Сохранить". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_buildings(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_BUILDINGS):
                with allure.step(
                        'Убедился в создании Здания, найдя строку с названием. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Здания, '
                                                                            'найдя строку с названием'
                                                                            '. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в создании Здания, найдя строку с названием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в создании Здания, '
                                                                        'найдя строку с названием'
                                                                        '. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    # Create params and data

    def click_link_params_data(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_LINK_PARAMS_DATA):
                with allure.step('Клик на строку "Параметры и данные". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Параметры и данные". '
                                                                            'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на строку "Параметры и данные". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на строку "Параметры и данные". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_initialize(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_CLICK_INITIALIZE):
                with allure.step('Клик на активную строку "Инициализировать". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Клик на активную строку "Инициализировать". '
                                       'Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку "Инициализировать". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на активную строку "Инициализировать". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_create_params_data(self):
        try:
            time.sleep(2.0)
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_CREATE_PARAMS_DATA):
                with allure.step(
                        'Убедился в инициализации параметров, найдя строку "Новый параметр"". Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в инициализации параметров, '
                                                                            'найдя строку "Новый параметр""'
                                                                            '. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в инициализации параметров, найдя строку "Новый параметр"". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в инициализации параметров, '
                                                                        'найдя строку "Новый параметр""'
                                                                        '. Провален.')
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)
