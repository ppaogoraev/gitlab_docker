import logging
import pytest
import allure
from Login_Page import SearchHelper
from Create_new_system import SearchHelperCNS


@pytest.mark.smoke_test_cv
@allure.feature('Smoke тестирование на Chrome. Создание Источника в системе инженерных сооружений.')
@allure.story('Проверка работы модуля "Добавить источник')
@allure.severity('Critical')
def test_create_new_system_chrome(test_chrome):
    try:
        logger = logging.getLogger('CV_logger')
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('test_create_system_source.txt')
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(ch)
        logger.addHandler(fh)
        cv_login = SearchHelper(test_chrome)
        cv_login.go_to_login()
        logging.debug('Перешел на страницу логина')
        # Логин и пароль изменять в зависимости от прав данного пользователя на сайте
        cv_login.login_input('tuser')
        logging.debug('Ввел верный логин и пароль пользователя c правом доступа на сайт')
        cv_login.login_password('951753')
        logging.debug('Ввел верный пароль')
        cv_login.click_login_button()
        logging.debug('Кликнул на кнопку логин')
        cv_login.assert_login_successfull()
        logging.debug('Убедился в том, что логин произошел')
        cv_create_system = SearchHelperCNS(test_chrome)
        cv_create_system.click_create_system_button()
        logging.debug('Убедился в том, кнопка нажалась')
        cv_create_system.input_name_new_system()
        logging.debug('Убедился в том, что название системы введено')
        cv_create_system.input_resource_new_system()
        logging.debug('Убедился в том, что нажалось на выпадающее меню "Ресурс".')
        cv_create_system.click_drop_resource()
        logging.debug('Убедился в том, что из выпадающего меню "Ресурс" выбран пункт "Питьевая вода".')
        cv_create_system.input_type_data()
        logging.debug('Убедился в том, что нажалось на выпадающее меню "Тип".')
        cv_create_system.click_drop_type()
        logging.debug('Убедился в том, что из выпадающего меню "Тип" выбран пункт "Водопотребление".')
        cv_create_system.click_create_zone()
        logging.debug('Убедился в том, что нажалось на выпадающее меню "Создать и добавиь зону".')
        cv_create_system.input_zone_name()
        logging.debug('Убедился в том, что название зоны водоснабжения введено')
        cv_create_system.input_comment_zone_name()
        logging.debug('Убедился в том, что комментарий введен.')
        cv_create_system.click_save_zone_data()
        logging.debug('Убедился в том, что кнопка "Сохранить зону" нажата.')
        cv_create_system.click_save_system_button()
        logging.debug('Убедился в том, что кнопка "Сохранить" нажата.')
        cv_create_system.assert_create_tile_system()
        logging.debug('Убедился в создании системы, найдя плитку с её названием на главной странице')
        cv_create_system.click_tiles_system()
        logging.debug('Убедился в том, что клик на плитку новой системы успешный.')
        # Create system Source
        cv_create_system.click_link_source()
        logging.debug('Убедился в том, что строка "Источники" нажата.')
        cv_create_system.click_create_system_source()
        logging.debug('Убедился в том, что кнопка "Добавить источник" нажата.')
        cv_create_system.input_source_name()
        logging.debug('Убедился в том, что заполнение данных "Наименование" успешный.')
        cv_create_system.click_source_type()
        logging.debug('Убедился в том, что нажался чек-бокс "Подземный.')
        cv_create_system.click_drop_zone()
        logging.debug('Убедился в том, клик на пункт "Выберите зону водоснабжения". Успешный.')
        cv_create_system.add_data_name_zone()
        logging.debug('Убедился в том, что клик на пункт "Ленинский район". Успешный.')
        cv_create_system.click_drop_stage()
        logging.debug('Убедился в том, что клик на пункт "Этап". Успешный.')
        cv_create_system.add_data_name_stage()
        logging.debug('Убедился в том, что клик на пункт "Водоподготовка". Успешный.')
        cv_create_system.click_drop_location()
        logging.debug('Убедился в том, что клик поле "Адрес". Успешный.')
        cv_create_system.add_data_location()
        logging.debug('Убедился в том, что заполнене поля "Адрес". Успешный.')
        cv_create_system.click_drop_list_location()
        logging.debug('Убедился в том, что кликнул на поле "Адрес". Успешный.')

        cv_create_system.wait_for_loading()

        cv_create_system.input_comments_source()
        logging.debug('Убедился в том, что заполнене поля "Комментарий". Успешный.')
        cv_create_system.click_save_data_source()
        logging.debug('Убедился в том, что нажимается кнопка "Сохранить". Успешный.')

        # Delete system
        cv_create_system.click_delete_system_button()
        logging.debug('Убедился в том, что кнопка "Удалить" нажата.')
        cv_create_system.click_confirm_delete_system()
        logging.debug('Убедился в том, что кнопка "Да,удалить" нажата.')
        cv_create_system.assert_delete_tile_system()
        logging.debug('Убедился в отсутствии плитки системы с её названием на главной странице')


    except:
        logging.exception('')
        logging.critical('TEST FAILED')
        exit(1)
