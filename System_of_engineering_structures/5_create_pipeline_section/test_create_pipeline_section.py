import logging
import pytest
import allure
from Login_Page import SearchHelper
from Create_new_system import SearchHelperCNS


@pytest.mark.smoke_test_cv
@allure.feature('Smoke тестирование на Chrome. Создание "Добавить участок сети" в системе инженерных сооружений.')
@allure.story('Проверка работы модуля "Добавить участок сети"')
@allure.severity('Critical')
def test_create_new_system_chrome(test_chrome):
    try:
        logger = logging.getLogger('CV_logger')
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('test_create_pipeline_section.txt')
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(ch)
        logger.addHandler(fh)

        # Шаги тестирования, начиная с логина

        cv_login = SearchHelper(test_chrome)
        cv_login.go_to_login()
        logging.debug('Перешел на страницу логина')

        # Log in
        cv_login.login_input('tuser')
        logging.debug('Ввел верный логин и пароль пользователя c правом доступа на сайт')

        cv_login.login_password('951753')
        logging.debug('Ввел верный пароль')

        cv_login.click_login_button()
        logging.debug('Кликнул на кнопку логин')

        cv_login.assert_login_successfull()
        logging.debug('Убедился в том, что логин произошел')

        cv_create_system = SearchHelperCNS(test_chrome)

        # Create system

        cv_create_system.click_create_system_button()
        logging.debug('Убедился в том, кнопка нажалась')

        cv_create_system.input_name_new_system()
        logging.debug('Убедился в том, что название системы введено')

        cv_create_system.input_resource_new_system()
        logging.debug('Убедился в том, что нажалось на выпадающее меню "Ресурс".')

        cv_create_system.click_drop_resource()
        logging.debug('Убедился в том, что из выпадающего меню "Ресурс" выбран пункт "Питьевая вода".')

        cv_create_system.input_type_data()
        logging.debug('Убедился в том, что нажалось на выпадающее меню "Тип".')

        cv_create_system.click_drop_type()
        logging.debug('Убедился в том, что из выпадающего меню "Тип" выбран пункт "Водопотребление".')

        cv_create_system.click_create_zone()
        logging.debug('Убедился в том, что нажалось на выпадающее меню "Создать и добавиь зону".')

        cv_create_system.input_zone_name()
        logging.debug('Убедился в том, что название зоны водоснабжения введено')

        cv_create_system.input_comment_zone_name()
        logging.debug('Убедился в том, что комментарий введен.')

        cv_create_system.click_save_zone_data()
        logging.debug('Убедился в том, что кнопка "Сохранить зону" нажата.')

        cv_create_system.click_save_system_button()
        logging.debug('Убедился в том, что кнопка "Сохранить" нажата.')

        cv_create_system.assert_create_tile_system()
        logging.debug('Убедился в создании системы, найдя плитку с её названием на главной странице')

        cv_create_system.click_tiles_system()
        logging.debug('Убедился в том, что клик на плитку новой системы успешный.')

        # Create system Source

        cv_create_system.click_link_source()
        logging.debug('Убедился в том, что строка "Источники" нажата.')

        cv_create_system.click_create_system_source()
        logging.debug('Убедился в том, что кнопка "Добавить источник" нажата.')

        cv_create_system.input_source_name()
        logging.debug('Убедился в том, что заполнение данных "Наименование" успешный.')

        cv_create_system.click_source_type()
        logging.debug('Убедился в том, что нажался чек-бокс "Подземный.')

        cv_create_system.click_drop_zone()
        logging.debug('Убедился в том, клик на пункт "Выберите зону водоснабжения". Успешный.')

        cv_create_system.add_data_name_zone()
        logging.debug('Убедился в том, что клик на пункт "Ленинский район". Успешный.')

        cv_create_system.click_drop_stage()
        logging.debug('Убедился в том, что клик на пункт "Этап". Успешный.')

        cv_create_system.add_data_name_stage()
        logging.debug('Убедился в том, что клик на пункт "Водоподготовка". Успешный.')

        cv_create_system.click_drop_location()
        logging.debug('Убедился в том, что клик поле "Адрес". Успешный.')

        cv_create_system.add_data_location()
        logging.debug('Убедился в том, что заполнене поля "Адрес". Успешный.')

        cv_create_system.click_drop_list_location()
        logging.debug('Убедился в том, что кликнул на поле "Адрес". Успешный.')

        cv_create_system.input_comments_source()
        logging.debug('Убедился в том, что заполнене поля "Комментарий". Успешный.')

        cv_create_system.click_save_data_source()
        logging.debug('Убедился в том, что нажимается кнопка "Сохранить". Успешный.')

        cv_create_system.wait_for_loading()

        # Create water network object

        cv_create_system.click_link_water_network_object()
        logging.debug('Убедился в том, что клик на строку "Объекты сети". Успешный.')

        cv_create_system.click_create_water_network_object()
        logging.debug('Убедился в том, что клик на строку "Добавить объект сети". Успешный.')

        cv_create_system.input_water_network_object_name()
        logging.debug('Убедился в том, что заполнение данных "Наименование" "НС-100". Успешный.')

        cv_create_system.click_drop_type_object()
        logging.debug('Убедился в том, что клик на строку "Тип объекта". Успешный.')

        cv_create_system.click_name_drop_type_object()
        logging.debug('Убедился в том, что клик на строку "Насосная станиция". Успешный.')

        cv_create_system.click_drop_zone_object()
        logging.debug('Убедился в том, что клик на строку "Зона водоснабжения". Успешный.')

        cv_create_system.click_name_zone_object()
        logging.debug('Убедился в том, что клик на строку "Ленинский район". Успешный.')

        cv_create_system.click_drop_stage_object()
        logging.debug('Убедился в том, что клик на пункт "Этап". Успешный.')

        cv_create_system.add_data_name_stage_object()
        logging.debug('Убедился в том, что клик на пункт "Транспортировка". Успешный.')

        cv_create_system.click_drop_location_object()
        logging.debug('Убедился в том, что клик на пункт "Адрес". Успешный.')

        cv_create_system.add_data_location_object()
        logging.debug('Убедился в том, что данные "Удмуртская респ., р-н Кезский, тер. СПК Свобода '
                      'Удмуртский Зязьгор" введены в поле. Успешный.')

        cv_create_system.click_drop_list_location_object()
        logging.debug('Убедился в том, что клик на пункт в списке "Удмуртская респ., р-н Кезский, тер. СПК Свобода '
                      'Удмуртский Зязьгор". Успешный.')

        cv_create_system.click_save_data_object()
        logging.debug('Убедился в том, что клик на пункт "Сохранить". Успешный.')

        cv_create_system.assert_create_network_object()
        logging.debug('Убедился в том, что "Объект сети" создан отображается. Успешный.')

        # Create consumer

        cv_create_system.click_link_consumer()
        logging.debug('Убедился в том, что клик на активную строку "Потербители". Успешный.')

        cv_create_system.click_create_consumer()
        logging.debug('Убедился в том, что клик на активную строку "Добавить потребителя". Успешный.')

        cv_create_system.input_consumer_name()
        logging.debug('Убедился в том, что данные "Химчистка Снежинка" в поле "Наименование" введены. Успешный.')

        cv_create_system.click_drop_zone_consumer()
        logging.debug('Убедился в том, что клик на пункт "Зона водоснабжения". Успешный.')

        cv_create_system.click_name_drop_zone_consumer()
        logging.debug('Убедился в том, что клик на пункт в списке зон "Ленинский район". Успешный.')

        cv_create_system.click_drop_location_consumer()
        logging.debug('Убедился в том, что клик на пункт "Адрес". Успешный.')

        cv_create_system.add_data_location_consumer()
        logging.debug('Убедился в том, что поле "Адрес" добавлены данные для поиска. Успешный.')

        cv_create_system.click_drop_list_location_consumer()
        logging.debug('Убедился в том, что клик на пункт в списке зон "Удмуртская респ., '
                      'р-н Кезский, д. Удмурт-Зязьгор, ул. Юбилейная". Успешный.')

        cv_create_system.click_save_data_consumer()
        logging.debug('Убедился в том, что клик на кнопку "Сохранить". Успешный.')

        cv_create_system.assert_create_consumer()
        logging.debug('Убедился в том, что "Потребитель" создан и отображается. Успешный.')

        # Create pipeline section

        cv_create_system.click_link_pipeline_section()
        logging.debug('Убедился в том, что клик на активную строку "Участки сети". Успешный')

        cv_create_system.click_create_pipeline_section()
        logging.debug('Убедился в том, что клик на активную строку "Добавить участок сети". Успешный')

        cv_create_system.input_pipeline_section_name()
        logging.debug('Убедился в том, что заполнение данных "Наименование" "Трубопровод 150". Успешный.')

        cv_create_system.click_pipeline_section_type()
        logging.debug('Убедился в том, что клик на чек-бокс "Активный. Успешный')

        cv_create_system.click_drop_start_section()
        logging.debug('Убедился в том, что клик на поле "Начало участка". Успешный')

        cv_create_system.add_data_drop_start_section()
        logging.debug('Убедился в том, что ввод данных "Начало участка" "НС-100". Успешный ')

        cv_create_system.click_drop_list_start_section()
        logging.debug('Убедился в том, что клик на пункт в списке "НС-100". Успешный.')

        cv_create_system.click_drop_end_section()
        logging.debug('Убедился в том, что клик на поле "Окончание участка". Успешный')

        cv_create_system.add_data_drop_end_section()
        logging.debug('Убедился в том, что  заполнение данных "Окончание участка" "Химчистка Снежинка". Успешный')

        cv_create_system.click_drop_list_end_section()
        logging.debug(
            'Убедился в том, что клик на пункт в списке "Химчистка Снежинка". Успешный.')

        cv_create_system.click_save_data_section()
        logging.debug('Убедился в том, что клик на кнопку "Сохранить". Успешный.')

        cv_create_system.assert_create_section()
        logging.debug('Убедился в создании участка сети "Трубопровод 150", найдя строку с названием. Успешный.')





        # # Delete system
        # cv_create_system.click_delete_system_button()
        # logging.debug('Убедился в том, что кнопка "Удалить" нажата.')
        # cv_create_system.click_confirm_delete_system()
        # logging.debug('Убедился в том, что кнопка "Да,удалить" нажата.')
        # cv_create_system.assert_delete_tile_system()
        # logging.debug('Убедился в отсутствии плитки системы с её названием на главной странице')
        # # cv_create_system.wait_for_loading()

    except:
        logging.exception('')
        logging.critical('TEST FAILED')
        exit(1)
